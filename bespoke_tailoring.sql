-- SATURDAY 04-01-2020
SHOW DATABASES;
USE bespoke_tailoring;
USE bespoke_tailoring2;
SHOW TABLES;
DESCRIBE product;
SELECT * FROM product;
-- NEW DATABASE
SHOW DATABASES;
USE college;
CREATE TABLE student(
id INT NOT NULL AUTO_INCREMENT,
fname VARCHAR(32) NOT NULL,
lname VARCHAR(32) NOT NULL,
dob DATE NOT NULL,
PRIMARY KEY(id)
);
-- UNIKE KEY id_teacher_uq (id)
CREATE TABLE teacher(
id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
fname VARCHAR(32) NOT NULL,
lname VARCHAR(32) NOT NULL
);
--
--
USE bespoke_tailoring2;
CREATE TABLE product(
id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
product_name VARCHAR(100) NOT NULL,
product_price DECIMAL(13,4) NOT NULL
);
CREATE TABLE `client`(
id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
fname VARCHAR(50) NOT NULL,
lname VARCHAR(50) NOT NULL
);
RENAME TABLE `client` TO customer;
CREATE TABLE `order`(
id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
client_id INT NOT NULL,
order_date DATE NOT NULL
);
RENAME TABLE `order` TO customer_order;
SHOW TABLES;

--add column, new column.
ALTER TABLE customer_order ADD COLUMN total DECIMAL(13,4) NOT NULL;
--ALTER TABLE customer_order ADD COLUMN total DECIMAL(13,4) NOT NULL after customer_name;
--ALTER TABLE customer_order ADD COLUMN total DECIMAL(13,4) NOT NULL first;

DESCRIBE customer_order;
RENAME TABLE customer_order TO invoice;
DESCRIBE invoice;

CREATE TABLE item(
invoice_id INT NOT NULL,
product_id INT NOT NULL,
quantity INT NOT NULL
);
SHOW TABLES;
DESCRIBE invoice;

CREATE TABLE tax(
id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
tax_name VARCHAR(100) NOT NULL
);
SHOW TABLES;
ALTER TABLE tax ADD COLUMN commentary VARCHAR(500);

CREATE TABLE tax_rate(
tax_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
rate TINYINT UNSIGNED NOT NULL
);	
DESCRIBE customer;
SHOW COLLATION;
SHOW VARIABLES LIKE 'character_set%';
SHOW VARIABLES LIKE 'collation%';
SHOW TABLES;
DESCRIBE invoice;

--change column name.
ALTER TABLE invoice CHANGE COLUMN client_id customer_id INT;
--change column name, modify, alter, mariadb 10.5.2;
--alter table unit_of_measure rename column unit_of_measure_id to physical_property_id;

--change column type, modify;
ALTER TABLE invoice MODIFY customer_id INT NOT NULL;

ALTER TABLE invoice ADD CONSTRAINT fk_customer_invoice FOREIGN KEY (customer_id) REFERENCES customer(id);
ALTER TABLE invoice DROP FOREIGN KEY fk_customer_invoice;
ALTER TABLE invoice ADD CONSTRAINT fk_customer_invoice FOREIGN KEY (customer_id) REFERENCES customer(id) ON UPDATE CASCADE ON DELETE CASCADE;

DESCRIBE item;
ALTER TABLE item ADD COLUMN id INT NOT NULL;
--delete a column.
ALTER TABLE item DROP COLUMN id;
ALTER TABLE item ADD COLUMN id INT NOT NULL FIRST;

-- ALTER TABLE item ADD PRIMARY KEY (id);
--change column type, modify.
ALTER TABLE item MODIFY COLUMN id INT AUTO_INCREMENT PRIMARY KEY;

ALTER TABLE item ADD CONSTRAINT fk_item_invoice FOREIGN KEY (invoice_id) REFERENCES invoice(id);
ALTER TABLE item ADD CONSTRAINT fk_item_product FOREIGN KEY (product_id) REFERENCES product(id);
ALTER TABLE item DROP FOREIGN KEY fk_item_invoice;
ALTER TABLE item DROP FOREIGN KEY fk_item_product;
ALTER TABLE item ADD CONSTRAINT fk_item_invoice FOREIGN KEY (invoice_id) REFERENCES invoice(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE item ADD CONSTRAINT fk_item_product FOREIGN KEY (product_id) REFERENCES product(id) ON DELETE CASCADE ON UPDATE CASCADE;

--delete table
--drop table my_table_name;
--drop table my_table_name, other_table;
--drop table if exists my_table_name;

SHOW TABLES;
DESCRIBE customer;
INSERT INTO customer (fname, lname) VALUE ('Adres', 'Villa'), ('Lionel', 'Messi'), ('Cristiano', 'Ronaldo'),
('Andrea', 'Fiallos'), ('Nicole', 'Kidman'), ('Emma', 'Stone');
SELECT * FROM customer;

DESCRIBE product;
INSERT INTO product (product_name, product_price) VALUES 
('SONY PLAYSTATION 4 SLIM 1TB BLK CONSOLE', 10990.4567),
('SONY PLAYSTATION DS4 (ANY COLOR) GAME CONTROLLER', 2990.1234),
('SONY PS CAMERA ACCESSORIES', 2990.6789),
('PS4-ACE COMBAT 7:SKIES UNKNOWN (R3) GAMES', 2599.3456),
('PS4-CALL OF DUTTY BLACK OPS 4(R3) GAMES', 3149.9854),
('PS4-GOD OF WAR 2018', 1099.6521),
('PS4-MARVEL\'S SPIDER-MAN', 1899.4267),
('PS4-MORTAL KOMBAT 11 (R3)', 2595.8428);
SELECT * FROM product;

DESCRIBE invoice;

--change column name.
ALTER TABLE invoice CHANGE COLUMN order_date invoice_date DATE NOT NULL;

SELECT CURDATE();
SELECT CURRENT_TIME();
SELECT current_timestamp();
SELECT NOW();
SELECT NOW() + 0; -- AS A NUMBER
-- tuesday 07-01-2020
SHOW DATABASES;
USE bespoke_tailoring2;
SHOW TABLES;
SELECT CURRENT_TIMESTAMP();
DESCRIBE invoice;
SELECT * FROM customer;
SELECT * FROM product;
-- total must be null
SELECT TIMESTAMP('2019-09-25 10:12:07');
SELECT TIMESTAMP('2019-10-15 15:15:00', '1:15:00');

--change column type.
ALTER TABLE invoice MODIFY COLUMN total DECIMAL(13,4);
INSERT INTO invoice (customer_id, invoice_date)  VALUES (5, TIMESTAMP('2019-01-03 09:46:00')),
(2, TIMESTAMP('2019-01-15 08:22:00')), (4, TIMESTAMP('2019-01-24 13:35:00')),
(6, TIMESTAMP('2019-02-07 10:07:00')), (3, TIMESTAMP('2019-02-12 11:10:00')),
(1, TIMESTAMP('2019-03-11 14:40:00')), (5, TIMESTAMP('2019-03-19 10:47:00')),
(2, TIMESTAMP('2019-04-05 09:31:00')), (6, TIMESTAMP('2019-04-06 13:48:00')),
(3, TIMESTAMP('2019-05-08 15:29:00'));
SELECT * FROM invoice;
## DELETE ALL VALUES
DELETE FROM invoice;

--change column name.
ALTER TABLE invoice CHANGE COLUMN invoice_date invoice_datetime DATETIME NOT NULL;
-- FIX NAME OF COLUMN invoice_date to invoice_datetime

INSERT INTO invoice (customer_id, invoice_datetime)  VALUES (5, TIMESTAMP('2019-01-03 09:46:00')),
(2, TIMESTAMP('2019-01-15 08:22:00')), (4, TIMESTAMP('2019-01-24 13:35:00')),
(6, TIMESTAMP('2019-02-07 10:07:00')), (3, TIMESTAMP('2019-02-12 11:10:00')),
(1, TIMESTAMP('2019-03-11 14:40:00')), (5, TIMESTAMP('2019-03-19 10:47:00')),
(2, TIMESTAMP('2019-04-05 09:31:00')), (6, TIMESTAMP('2019-04-06 13:48:00')),
(3, TIMESTAMP('2019-05-08 15:29:00'));

DESCRIBE item;
INSERT INTO item (invoice_id, product_id, quantity) VALUES (21, 6, 3), (21, 1, 4), (21, 2, 6), (21, 3, 9),
(22, 5, 5), (22, 8, 14), (22, 7, 11), (22, 4, 2), (22, 1, 14), (23, 6, 5), (23, 7, 11), (23, 2, 7),
(24, 8, 18), (24, 5, 5), (24, 1, 16), (24, 2, 8), (24, 7, 4), (25, 5, 8), (25, 1, 17), (25, 2, 6),
(26, 1, 4), (26, 2, 7), (26, 5, 7), (27, 1, 3), (27, 2, 11), (27, 3, 7), (27, 4, 12), (27, 6, 2),
(28, 1, 5), (28, 2, 4), (28, 6, 7), (29, 1, 3), (29, 2, 8), (29, 3, 7), (29, 5, 14), (30, 1, 7),
(30, 2, 18), (30, 3, 2), (30, 4, 16), (30, 5, 7);
SELECT * FROM item;
-- 12-01-2020
SHOW DATABASES;
USE bespoke_tailoring2;
SHOW TABLES;
SELECT * FROM item;
SELECT * FROM product;
SELECT * FROM item, product WHERE item.product_id = product.id ORDER BY item.invoice_id;

SELECT item.invoice_id, product.product_name, item.quantity, product.product_price 
FROM item, product 
WHERE item.product_id = product.id 
ORDER BY item.invoice_id;

SELECT i.invoice_id, p.product_name, i.quantity, p.product_price,  i.quantity*p.product_price as subtotal
FROM item as i, product as p 
WHERE i.product_id = p.id 
ORDER BY i.invoice_id;

-- total of each invoice
SELECT i.invoice_id, SUM(i.quantity*p.product_price) as total
FROM item as i, product as p 
WHERE i.product_id = p.id 
GROUP BY i.invoice_id
ORDER BY i.invoice_id ASC;

-- only invoice 25
SELECT i.invoice_id, SUM(i.quantity*p.product_price) as total
FROM item as i, product as p 
WHERE i.product_id = p.id 
GROUP BY i.invoice_id
HAVING i.invoice_id = 25
ORDER BY i.invoice_id DESC;

--  total sale
SELECT SUM(i.quantity*p.product_price) as total
FROM item as i, product as p 
WHERE i.product_id = p.id;

-- MAX TOTAL VALUE
SELECT MAX(total) as mymax, MIN(total) as mymin, FLOOR(AVG(total)) as myavg
FROM (
SELECT i.invoice_id, SUM(i.quantity*p.product_price) as total
FROM item as i, product as p 
WHERE i.product_id = p.id 
GROUP BY i.invoice_id
ORDER BY i.invoice_id ASC
) AS t;


