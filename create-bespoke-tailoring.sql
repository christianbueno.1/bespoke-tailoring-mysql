-- comment single line
/*
Name:Christian Bueno
database user: tom
consideration:
-check item>quantity value, 0.36 kilos

bespoke-tailoring git:
-check quantity column type-
-fix item-table, set FK-
-calculate item_total, creating a trigger-
-add resource folder-
-add insert.sql-
-add categories table, tax table-
-add physical_property table-
--rename table, unit to unit_of_measure table-
--FIX PRODUCT TABLE, UNIT_PRICE, UNIT(KG, HOUR, m, Litre() water tanks, gallon(gal), dozen(doz)); SKU stock-keeping unit;-
--add color, size tables, insert values- 
--add stock column.
--product table, take care of item table and total.
--add subtotal-table




produt table:
water tanks, size: 10000 Litre; best material: Poly,  Steel; price: $1700 - $2500
unit abbreviation list, link: https://converticious.com/list-of-units/



database bespoke_tailoring sql
invoice_total:
-subtotal_sin_impuestos, decimal, Sumatoria( cant * precio)
desglose del subtotal
-subtotal_12, decimal, aquellos que tendran IVA
-subtotal_0, decimal, IVA 0% 
-subtotal_no_objeto_iva, decimal,(EJEMPLO: HERNECIA)
-subtotal_exento_iva, decimal, (ministerio produccion, menajes de casa y equipo de trabajo, donaciones provenientes del exterior)
---
-valor_ice, decimal, impuesto a los consumos especiales
-iva_12, decimal, subtotal_12*0.12
-valor_irbpnr, decimal, impuesto redimible a las botellas plasticas no retornables IRBPNR
-propina_10, decimal, 10%
-valor_total, decimal

next database:
-food
-clothing store
-

*/

drop database if exists bespoke_tailoring;
create database bespoke_tailoring;
use bespoke_tailoring;

create table customer(
	id int unsigned auto_increment primary key,
	fname varchar(50) not null,
	lname varchar(50) not null
);

create table tax (
	id int unsigned auto_increment primary key,
	tax_name varchar(200) not null,
	tax_symbol varchar(50) not null,
	tax_value decimal(5,4) not null
);


/*

category, subcategory  https://stackoverflow.com/questions/31623575/database-design-categories-and-sub-categories

category:
[Bien, Servicio]
[
clothing, music, posters, Computers, computers & Accesories, software, beauty picks(maquillajes), High-end foundation makeup, video games, baby, Toys & Games, Stuffed Animals & Toys (peluches), Electronics, Women's Fashion, Men's Fashion, Girls' Fashion, Boy's Fashion, Home and Kitchen, Dog beds, Pet Supplies[Dry Dog Food, Dry Cat Food, Canned Dog Food]
]
[
top categories, amazon top sellers, get fit at home, find your ideal tv, top beauty & personal care products, 
]
[
-t-shirts, hoodies, pants
-accesories:jewelry, shoes[Heels, Boots], eyewear, watches,
]



*/
create table category (
	id int unsigned auto_increment primary key,
	category_name varchar(200) not null,
	parent_category_id int
);
--alter table category add column parent_category_id int;
--UNIT OF MEASURE, SI units
/*

Physical properties and units of measure:
Length/Distance: km, m, cm
Mass/Weight: kg, g,
Capacity/Volume: L, ml, qt
Time/Working Time : s, h, Days 
Pressure: pascal, atmosphere, bar, torr
Temperature: kelvin(k), degress Celsius (C)
Unit: Units, Dozens

odoo Open Source ERP CRM
Unit:
Weight:
Working Time
Length/Distance
Volume

*/
create table color(
	id int unsigned auto_increment primary key,
	color_name varchar(50) not null,
	color_value varchar(20) not null
);
--alter table color add column color_value varchar(20);
--alter table color modify color_value varchar(20) not null;
--update color set color_value = '#000000' where id = 1; --black
--update color set color_value = '#ffffff' where id = 2; --white
--update color set color_value = '#0000ff' where id = 3; --blue

create table the_size (
	id int unsigned auto_increment primary key,
	size_name varchar(50) not null,
	size_value varchar(50) not null
);
--alter table the_size add column size_value varchar(20) not null;


create table physical_property (
	id int unsigned auto_increment primary key,
	physical_property_name varchar(50)
);
create table unit_of_measure (
	id int unsigned auto_increment primary key,
	physical_property_id int unsigned not null,
	unit_name varchar(200) not null,
	unit_symbol varchar(50) not null,
	constraint `fk_unit_of_measure_physical_property`
		foreign key (physical_property_id) references physical_property(id)
		on delete cascade
		on update restrict
);
--change table name.
--rename table unit to unit_of_measure;
--alter table unit_of_measure add column physical_property_id int unsigned not null after id;
--change column name, modify, alter, mariadb 10.5.2
--alter table unit_of_measure rename column unit_of_measure_id to physical_property_id; 
--alter table, add FK
--alter table unit_of_measure add constraint fk_unit_of_measure_physical_property foreign key (physical_property_id) references physical_property(id) on delete cascade on update restrict;
--drop constraint of a table.
--alter table unit_of_measure drop constraint fk_unit_of_measure_physical_property;

/*
--fixing


*/
CREATE TABLE product(
	id INT unsigned AUTO_INCREMENT PRIMARY KEY,
	product_name VARCHAR(100) NOT NULL
);
--alter table product drop column product_price;
--column deleted: product_price DECIMAL(13,4) NOT NULL

create table invoice(
	id int unsigned auto_increment primary key,
	customer_id int unsigned not null,
	invoice_datetime datetime not null,
	constraint `fk_invoice_customer`
		foreign key (customer_id) references customer (id)
		on delete cascade
		on update restrict
);

create table item(
	id int unsigned auto_increment primary key,
	invoice_id int unsigned not null,
	product_id int unsigned not null,
	quantity decimal(13,4) not null,
	item_total decimal(13,4) default 0.0,
	constraint `fk_item_invoice`
		foreign key (invoice_id) references invoice (id)
		on delete cascade
		on update restrict,
	constraint `fk_item_product`
		foreign key (product_id) references product (id)
		on delete cascade
		on update restrict
);



/*
create table subtotal(
	id int unsigned auto_increment primary key,
	invoice_id int unsigned not null,
	
);
create table invoice(
	id int unsigned auto_increment primary key,
	customer_id int unsigned not null,
	invoice_datetime datetime not null,
	total decimal(13,4) default 0.0
);


insert into invoice (customer_id, invoice_datetime) values (1, current_timestamp());
insert into item (invoice_id, product_id, quantity, item_total) values (1,1,5,100);

select i.product_id, p.product_name, i.quantity, p.product_price, i.quantity * p.product_price item_total2 from item i join product p on i.product_id = p.id;


delimiter $$
drop trigger if exists `calculate_item_total`$$
create trigger calculate_item_total
before insert
on item
for each row
begin
set new.item_total = (select p.product_price * new.quantity from product p where p.id = new.product_id);
end$$
delimiter ;


--inner join = join
select i.quantity * p.product_price as item_total2 from item i join product p on i.product_id = p.id where i.id = new.id ;

--before, after:insert, update, delete

--ok
create trigger calculate_item_total before insert on item for each row set new.item_total = (select p.product_price * new.quantity from product p where p.id = new.product_id);
*/



insert into customer (fname, lname) values
('Christian', 'Bueno'),
('Juan', 'Reyes'),
('Camilo', 'Reyes'),
('Roger', 'Reyes'),
('Andrea', 'Muñoz'),
('Christian', 'Bale'),
('Karol', 'Pereira');

insert into product (product_name, product_price) values
('pantalon azul casimir', 35),
('camisa guayabera blanca', 75),
('terno casimir ingles', 350),
('TARJETA GRAFICA NVIDIA RTX 2060 6GB ENERO 2019 EC', 435),
('TARJETA GRAFICA NVIDIA GEFORCE RTX 3090 GPU MEMORY 24GB SG GDDR6X MARZO 2021', 1799.95),
('TARJETA GRAFICA NVIDIA GEFORCE RTX 3080 GPU MEMORY 10GB MARZO 2021', 699.99),
('MONITOR LG 4K 42 INCH MARZO 2021', 696.99);

insert into tax (tax_name, tax_symbol, tax_value) values
('IMPUSTO AL VALOR AGREGADO', 'IVA 12%', 0.12),
('IMPUESTO AL VALOR AGREGADO 0%', 'IVA 0%', 0.0),
('NO OBJETO DE IVA', 'NO OBJETO DE IVA', 0.0),
('EXENTO DE IVA', 'EXENTO DE IVA', 0.0);
('IMPUESTO REDIMIBLE A LAS BOTELLAS PLASTICAS NO RETORNABLES IRBPNR', 'IRBPNR', 0.10),
('PROPINA 10%', 'PROPINA 10%', 0.10);
-- delete from tx; --all
-- alter table tax modify column tax_value decimal(5,4) not null;
-- update tax set tax_value = 0.15 where id = ?;

insert into category (id, category_name, parent_category_id) values
(1, 'BIEN', NULL),
(2, 'SERVICIO', null),
(3, 'COMPUTERS', NULL),
(4, 'ELECTRONICS', NULL),
(5, 'MEN\'S FASHION', 35),
(6, 'WOMEN\'S FASHION', 35),
(7, 'PANTS', 35),
(8, 'MEN\'S PANT', 5),
(9, 'WOMEN\'S PANT', 6),
(10, 'T-SHIRTS', 35),
(11, 'SHIRTS', 35),
(12, 'HOODIES', 35),
(13, 'JEWELRY', NULL),
(14, 'SHOES', NULL),
(15, 'SOFTWARE', NULL),
(16, 'VIDEO GAMES', NULL),
(17, 'BABY', NULL),
(18, 'TOYS & GAMES', NULL),
(19, 'STUFFED ANIMALS & TOYS', NULL),
(20, 'GIRLS\' FASHION', NULL),
(21, 'BOY\'S FASHION', NULL),
(22, 'HOME', NULL),
(23, 'KITCHEN', NULL),
(24, 'DOG BEDS', NULL),
(25, 'PET SUPPLIES', NULL),
(26, 'DRY DOG FOOD', 25),
(27, 'DRY CAT FOOD', 25),
(28, 'CANNED DOG FOOF', 25),
(29, 'CANNED CAT FOOD', 25),
(30, 'BEAUTY & PERSONAL CARE PRODUCTS', NULL),
(31, 'EYEWEAR', NULL),
(32, 'WATCHES', NULL),
(33, 'COMPUTER ACCESORIES', 3),
(34, 'BEAUTY PRODUCTS', NULL),
(35, 'CLOTHING', NULL),
(36, 'MUSIC', NULL)
(37, 'MEN\'S SHORT', 5);
--update category set category_name = 'SHIRTS' where id = 5;
--select * from category where id = 35 || parent_category_id in (35, 5, 6);
--select * from category c, category c2, category c3 where c.id = c2.parent_category_id && c2.id = c3.parent_category_id;
--select * from category c, category c2, category c3 where c.id = c2.parent_category_id && c2.id = c3.parent_category_id order by c2.category_name;

insert into physical_property (physical_property_name) values
('LENGTH/DISTANCE'),
('MASS/WEIGHT'),
('CAPACITY/VOLUME'),
('WORKING TIME'),
('PRESSURE'),
('TEMPERATURE'),
('UNIT');

/*
--unit symbol with 1 before symbol.
insert into unit_of_measure (physical_property_id, unit_name, unit_symbol) values
(2, '1 KILOGRAM', '1 Kg'),
(3, '1 LITRE', '1 L'),
(1, '1 METRE', '1 m'),
(4, '1 HOUR', '1 h'),
(3, '1 GALLON', '1 gal'),
(2, '37.5 LIBRA BAG', '37.5 lb BAG'),
(7, 'A DOZEN', 'doz'),
(4, '1 SECOND', '1 s'),
(7, '1 UNIT', 'UNIT' );
*/
insert into unit_of_measure (physical_property_id, unit_name, unit_symbol) values
(2, '1 KILOGRAM', 'Kg'),
(3, '1 LITRE', 'L'),
(1, '1 METRE', 'm'),
(4, '1 HOUR', 'h'),
(3, '1 GALLON', 'gal'),
(2, '37.5 LIBRA BAG', '37.5 lb'),
(7, 'A DOZEN', 'doz'),
(4, '1 SECOND', 's'),
(7, '1 UNIT', 'UNIT' );
--delete all data.
--delete from unit_of_measure;

insert into color (color_name) values
('BLACK'),
('WHITE'),
('BLUE');

insert into the_size (size_name) values
('XS'),
('S'),
('M'),
('L'),
('XL'),
('XXL');

insert into invoice (customer_id, invoice_datetime) values (1, current_timestamp());
--insert into item (invoice_id, product_id, quantity, item_total) values (1,1,5,100);
