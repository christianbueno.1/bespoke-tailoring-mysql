delimiter $$
drop trigger if exists `calculate_item_total`$$
create trigger calculate_item_total
before insert
on item
for each row
begin
set new.item_total = (select p.product_price * new.quantity from product p where p.id = new.product_id);
end$$
delimiter ;

